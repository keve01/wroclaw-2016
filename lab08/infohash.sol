// 
// This contract allows a group of publishers to advertise
// their respective information content feeds.
// The info feeds are modeled as human-readable feed name, associated with
// a hash that represents a file in IPFS. In other words, 
// the content is assumed to be represented by a hash to a file in IPFS.
// The hash of that file is stored in the contract. 
// The file may point to the previous version of the feed hash 
// - this way the announcement feed may form a simple chain of data.
//
// The publisher is characterized by publisher address. 
// Each publisher (address) can have multiple named information feeds. 
// Each ino feed must have unique names. 
// The function register() allows creation of new feed for the publisher. 
// Feed name collision is not verified - information
// feed names are assumed to be globally unique. 
// We assume that only the owner of the feed can issue the updates. 
// 


/// InfoHash - mapping info feed name to info feed hash in IPFS filesystem
/// @author Mariusz Nowostawski
contract InfoHash {
    
    /// publisher address mapped to a human-readable info feed name
    mapping (string => address) publishers;
    
    /// map of info name to the IPFS file hash
    mapping (string => string) feeds;
    
    /// advertise the new entry to all subscribers
    /// @param name the name of the info feed
    /// @param hash the hash of the info feed
    event NewInfoHashPublished(string name, string hash);
    
    
    /// register the new publisher's info feed name
    function register(string name) public {
        // if name already registered, we throw
        if (publishers[name] != address(0x0)) throw;
        
        publishers[name] = msg.sender;
    }
    
    /// publish new info hash for a given infoName
    function publish(string infoName, string newHash) public {
        // only owner can publish
        if (publishers[infoName] != msg.sender) throw;
        
        feeds[infoName] = newHash;
        NewInfoHashPublished(infoName, newHash);
    }
    
    /// check the latest hash for a given infoName
    function getInfoHash(string infoName) public returns (string) {
        return feeds[infoName];
    }
    
}

