import {Component} from 'angular2/core';
import {Notification} from './notification';

@Component({

	selector: 'hello-world',
	directives: [Notification],

  template: `
  <div><notification [name]="yourName" [notif]="yourNotif" [pattern]="yourPattern"></notification>   </div>
    <label>Put in your name:</label>
 	<input type="text" [(ngModel)]="yourName" placeholder="Enter a name here">
    <label>Put in the pattern to your name:</label>
    <input type="text" [(ngModel)]="yourPattern" placeholder="Enter a pattern here">
    <label>Put in notification when Name is same with a pattern (appears before name):</label>
    <input type="text" [(ngModel)]="yourNotif" placeholder="Enter a notification text here">
	<hr> 
 	<h1 [hidden]="!yourName"><small>Hello</small> {{yourName}}! <big>Nice to meet you!</big></h1>
`
})

export class HelloWorld {
  // Declaring the variable for binding with initial value
  yourName: string = '';
  yourPattern: string = '';
  yourNotif: string = '';
}
