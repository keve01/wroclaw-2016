import {Component, Input} from 'angular2/core';

@Component({

  selector: 'notification',

  template: `
	<h1 [hidden]="name!=pattern">{{notif}} {{name}}</h1>
	<h1 [hidden]="name!='Mariusz'">Mariusz is lecturing in C025</h1>
`
})

export class Notification {
  @Input() name: string;
  @Input() pattern: string;
  @Input() notif: string;
}
